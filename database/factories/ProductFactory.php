<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {

        $filepath = 'public/storage/products';
        //  if(!file::exists($filepath)){
        //      file::makeDirectory($filepath);
        //  }

        return [
            'title' => $this->faker->sentence(),
            'description' => $this->faker->realText(500), 
            'price' => $this->faker->numberBetween(1000, 2000),
            'image' =>$this->faker->image($filepath , 250 , 300)
           
        ];
    }

   
}
