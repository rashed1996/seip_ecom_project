<x-backend.layouts.master>


    <div class="container-fluid px-4">
                            <h1 class="mt-4">Products</h1>
                            <ol class="breadcrumb mb-4">
                                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                <li class="breadcrumb-item active">Tables</li>
                            </ol>
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-table me-1"></i>
                                    products details
                                    <a href="{{route('products.index')}}" class="btn btn-sm btn-primary">Products list</a>
                                </div>
                                <div class="card-body">
                                <h1>Title: {{ $product->title}}</h1>
                                <p> Description  {{ $product->description}}</p>
                                <p>Price: {{$product->price}}</p>
                                <img src="{{ asset('storage/products/'.$product->image)}}"/>
                                <p> Created At:{{$product->created_at->diffForhumans()}}</p>
                                <p> Updated At:{{$product->updated_at->diffForhumans()}}</p>
                                </div>
                            </div>
                        </div>
    
    
    </x-backend.layouts.master>
    
    
    
    