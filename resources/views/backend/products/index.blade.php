<x-backend.layouts.master>


<div class="container-fluid px-4">
    <h1 class="mt-4">Products</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Tables</li>
    </ol>
    <!-- <div class="card mb-4">
        <div class="card-body">
            DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the
            <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>
            .
        </div>
    </div> -->
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            product list
            <a href="{{route('products.trush')}}" class="btn btn-sm btn-info">Trush List</a>
            <a href="{{route('products.create')}}" class="btn btn-sm btn-primary"> Add new</a>
        </div>
        <div class="card-body">

            @if(Session::has('message'))
            <p class="alert alert-success">{{session::get('message')}}</p>
            @endif
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Title</th>
                        <th>Price</th>
                        {{-- <th>Description</th> --}}
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>

        @foreach($products as $product)
                <tr>
                <td>{{ $loop->iteration}}</td>
                <td>{{ $product->title}}</td>
                <td>{{$product->price}}</td>
                {{-- <td>{{$product->description}}</td> --}}
                <td>

                    <a  class="btn btn-info btn-sm" href="{{ route('products.show',['id'=>$product->id])}}"> Show </a>
                        
                    <a  class="btn btn-warning btn-sm" href="{{ route('products.edit',['id'=>$product->id])}}"> Edit </a>

                    <form action="{{ route('products.destroy',['id'=>$product->id])}}" method="POST"
                       style="display: inline" >
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm ('are you sure for delete')">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach

                    
                </tbody>
            </table>
        </div>
    </div>
</div>


</x-backend.layouts.master>



