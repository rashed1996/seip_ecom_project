
<x-frontend.layouts.master>

<div class="bg-light p-5 rounded">
<div class="row row-cols-1 row-cols-md-3 mb-3 text-center">

@foreach ($products as $product)
      <div class="col">
        <div class="card mb-4 rounded-3 shadow-sm">
          <div class="card-header py-3">
            <img src=" {{ asset('storage/products/'.
              $product->image)}}" />
          </div>
          <div class="card-body">

          <!-- dd("Str::limit('The quick brown fox jumps over the lazy dog', 20)"); -->
           <a href=""><h4 class="card-title pricing-card-title">{{Str::limit($product->title, 100) }}

           </h4></a>
             <p>{{$product->price}}</p>
            <button type="button" class="w-100 btn btn-lg btn-outline-primary">Add to Card </button>
          </div>
        </div>
      </div>
 @endforeach
 </div>
     {{$products->links() }}
  </div>
  
</x-frontend.layouts.master>