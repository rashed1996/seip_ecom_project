<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Faker\Provider\Image;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Echo_;




class productController extends Controller
{
    function contact()
    {

        return view('contact');



    }

    function home()
    {

       echo "controller home";



    }
    function edit($id)
    {

        $product= Product::findorfail($id);
        
        return view('backend.products.edit', compact('product'));  


    }
    function update(Request $request ,$id)
    {

        try
        {
            $product= Product::findorfail($id);

            $product->update([
  
                  'title'=>$request->title,
                  'price'=>$request->price,
                  'description'=>$request->description,
              ]);
  
  
         return redirect()->route('products.index')->withMessage('successfully updated !');

        }
      
        catch(QueryException $e ){
       
            return redirect()->back()->withInput()->withErrors($e->getMessage());
               //  dd($e->getMessage());
       }


    }

    public function show($id)
    {


        // $product = Product::where('id', '=', $id)->first();

        $product= Product::findorfail($id);

        //  return view('backend.products.show'[
        //      'product' => $product
        //  ]); 
         return view('backend.products.show' , compact('product'));


    }

    public function index()
    {
        // $products=Product::all();
        $products=Product::orderBy('id','desc')->get();
        return view('backend.products.index', compact('products'));


    }

    public function create()

    {
        return view('backend.products.create');


    }

    public function store(ProductRequest $request)
    {

        try{

            $requestdata=$request->all();

            if($request->hasFile('image')){

                $file=$request->File('image');

                // dd($file->getclientOriginalExtension());

                $filename= time() . '.' . $file->getclientOriginalExtension();
           
                $image= Image::make($request->file('image'))
                ->resize(300,200)
                ->save(storage_path().'/app/public/products'.$filename);


                $requestdata['image']=$filename; 


            }

            //  $request->validate([
            //     'title' => 'required|min:4|unique:products,title',
            //     // 'price' => 'required|numeric',
            // ]);

            // dd('request validated');

            // $postdata=$request->all();
            

            // $product = new Product();
            // $product->title = $request->title;
            // $product->price = $request->price;
            // $product->description = $request->description;
            // $product->save();

            // DB::table('products')->insert([

            //     'title'=>$request->title,
            //     'price'=>$request->price,
            //     'description'=>$request->description,
            // ]);

            dd($requestdata);
            
            
            Product::create($requestdata);


            // Session::flush('message', 'successfully saved ');

            // dd($postdata);
            // Product::create([
    
            //     'title'=>$postdata['title'],
            //     'price'=>$postdata['price'],
            //     'description'=>$postdata['description']
            // ]);
            return redirect()->route('products.index')->withMessage('successfully saved !');
        }
        catch(QueryException $e ){
       
             return redirect()->back()->withInput()->withErrors($e->getMessage());
                //  dd($e->getMessage());
        }

    }
    public function destroy($id)
    {
      $product= Product::findorfail($id);
      $product->delete();
      return redirect()->route('products.index')->withMessage('successfully deleded !');


    }

    public function trush()
    {
      $products= Product::onlyTrashed()->get();

      return view('backend.products.trush',compact('products'));


    }
    public function restore($id)
    {
       Product::withTrashed()->where('id',$id)->restore(); 
        return redirect()->route('products.trush')->withMessage('successfully restore');
    }
    public function delete($id)
    {
       Product::withTrashed()
           ->where('id',$id)
           ->forceDelete(); 
        return redirect()->route('products.trush')->withMessage('successfully deleted');
    }

 









}
