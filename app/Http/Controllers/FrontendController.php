<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function welcome()
    {
         $products = Product::paginate(9);
        return view('welcome', compact('products'));

    }
}
